package web_shop_basic.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import web_shop_basic.dto.AdministratorDto;
import web_shop_basic.models.Administrator;
import web_shop_basic.services.GenericService;

@RestController
@RequestMapping(path = "/administrator")
public class AdministratorController extends GenericController<AdministratorDto, Administrator, Long> {

	public AdministratorController(GenericService<Administrator, Long> service) {
		super(service, AdministratorDto.class);
		// TODO Auto-generated constructor stub
	}

}
