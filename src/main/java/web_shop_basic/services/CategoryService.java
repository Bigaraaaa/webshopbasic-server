package web_shop_basic.services;

import org.springframework.stereotype.Service;

import web_shop_basic.models.Category;

@Service
public class CategoryService extends GenericService<Category, Long> {

}
