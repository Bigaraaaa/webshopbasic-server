package web_shop_basic.models;

public enum  OrderStatus {
    unfulfilled,
    fulfilled
}
