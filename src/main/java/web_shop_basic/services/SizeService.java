package web_shop_basic.services;

import org.springframework.stereotype.Service;

import web_shop_basic.models.Size;

@Service
public class SizeService extends GenericService<Size, Long> {

}
