package web_shop_basic.dto;

public class AccountDataDto extends AbstractDto {

	private String name;
	private String surname;
	private String email;
	
	public AccountDataDto() {
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	
}
