package web_shop_basic.services;

import org.springframework.stereotype.Service;

import web_shop_basic.models.ProductDetails;

@Service
public class ProductDetailsService extends GenericService<ProductDetails, Long> {

}
