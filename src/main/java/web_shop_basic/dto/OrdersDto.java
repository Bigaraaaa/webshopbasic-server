package web_shop_basic.dto;

import java.util.ArrayList;
import java.util.List;

public class OrdersDto extends AbstractDto {

	private Long id;
	private String name;
	private String surname;
	private String email;
	private String street;
	private String number;
	private String floor;
	private String apartmant;
	private String additionalInformation;
	private Double sum;
	private List<CartDto> cart = new ArrayList<CartDto>();

	public OrdersDto() {

	}

	public Double getSum() {
		return sum;
	}

	public void setSum(Double sum) {
		this.sum = sum;
	}

	public List<CartDto> getCart() {
		return cart;
	}

	public void setCart(List<CartDto> cart) {
		this.cart = cart;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getFloor() {
		return floor;
	}

	public void setFloor(String floor) {
		this.floor = floor;
	}

	public String getApartmant() {
		return apartmant;
	}

	public void setApartmant(String apartmant) {
		this.apartmant = apartmant;
	}

	public String getAdditionalInformation() {
		return additionalInformation;
	}

	public void setAdditionalInformation(String additionalInformation) {
		this.additionalInformation = additionalInformation;
	}
}
