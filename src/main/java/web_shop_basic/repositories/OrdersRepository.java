package web_shop_basic.repositories;

import org.springframework.stereotype.Repository;

import web_shop_basic.models.Orders;

@Repository
public interface OrdersRepository extends GenericRepository<Orders, Long> {

}
