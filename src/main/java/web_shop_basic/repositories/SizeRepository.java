package web_shop_basic.repositories;

import org.springframework.stereotype.Repository;

import web_shop_basic.models.Size;

@Repository
public interface SizeRepository extends GenericRepository<Size, Long> {

}
