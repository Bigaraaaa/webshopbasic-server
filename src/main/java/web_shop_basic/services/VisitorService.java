package web_shop_basic.services;

import java.util.HashSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import web_shop_basic.models.UserPermission;
import web_shop_basic.models.Visitor;
import web_shop_basic.repositories.PermissionRepository;
import web_shop_basic.repositories.VisitorRepository;

@Service
public class VisitorService extends GenericService<Visitor, Long> {

	@Autowired
	private VisitorRepository visitorRepository;
	
	@Autowired
	private PermissionRepository permissionRepository;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Override
	public Visitor add(Visitor model) {
		model.getAccountData().setPassword(passwordEncoder.encode(model.getAccountData().getPassword()));
		model = visitorRepository.save(model);
		
		model.getAccountData().setUserPermission(new HashSet<UserPermission>());
		model.getAccountData().getUserPermission().add(new UserPermission(null, model.getAccountData(), permissionRepository.findById(2l).get()));
		visitorRepository.save(model);
		return super.add(model);
	}
}
