package web_shop_basic.dto;

public class SizeDto extends AbstractDto {

	private Double price;
	private String amount;
	
	public SizeDto() {
		
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}
	
	
}
