package web_shop_basic.controllers;

import java.io.IOException;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;

import web_shop_basic.dto.ProductDto;
import web_shop_basic.models.Product;
import web_shop_basic.models.ProductDetails;
import web_shop_basic.services.FileService;
import web_shop_basic.services.GenericService;
import web_shop_basic.services.ProductService;

@RestController
@RequestMapping(path = "/product")
public class ProductController extends GenericController<ProductDto, Product, Long> {
	
	@Autowired
	private ProductService productService;
	
	@Autowired
	private FileService fileService;
	
	@Autowired
	private ModelMapper modelMapper;

	public ProductController(GenericService<Product, Long> service) {
		super(service, ProductDto.class);
		// TODO Auto-generated constructor stub
	}
	
//	@RequestMapping(value = "/save-product", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
//	public ResponseEntity<Product> saveProduct(@RequestPart("productImage") Optional<MultipartFile> file, @RequestPart("data") String productString) throws IOException {
//		ProductDetails product = new ObjectMapper().readValue(productString, ProductDetails.class);
//		if(file.isPresent()) {
//			fileService.saveProductImage(file.get(), "product_" + product.getName(), product);
//		}
//		productService.add(product);
//		return new ResponseEntity<Product>(product, HttpStatus.CREATED);
//	}
//	
//	@RequestMapping(value="/edit/{id}", method=RequestMethod.PUT, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
//    public ResponseEntity<Product> editProduct(@PathVariable Long id, @RequestPart("productImage") Optional<MultipartFile> file, @RequestPart("data") String productString) throws IOException {
//		Product product = new ObjectMapper().readValue(productString, Product.class);
//		if(file.isPresent()) {
//			fileService.saveProductImage(file.get(), "product_" + product.getName(), product);
//		}
//		productService.add(product);
//		return new ResponseEntity<Product>(product, HttpStatus.CREATED);
//    }

}
