package web_shop_basic.services;

import java.io.Serializable;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import web_shop_basic.models.Entities;
import web_shop_basic.repositories.GenericRepository;

@Service
public abstract class GenericService<T extends Entities<T, ID>, ID extends Serializable> {

    @Autowired
    protected GenericRepository<T, ID> repository;
    
    public GenericRepository<T, ID> getRepository() {
    	return this.repository;
    }

    public void setRepository(GenericRepository<T, ID> repository) {
    	this.repository = repository;
    }

    public Page<T> findAll(Pageable pageable) {
    	return this.repository.findAll(pageable);
    }

    public Iterable<T> findAll() {
    	return this.repository.findAll();
    }

    public Optional<T> findById(ID id) {
    	return this.repository.findById(id);
    }

    public void save(T model) {
    	this.repository.save(model);
    }
    
    public T add(T model) {
    	System.out.println(model);
    	return this.repository.save(model);
    }

    public void delete(ID id) {
    	Optional<T> t = this.repository.findById(id);
    	if(t.isPresent()) {
    		this.repository.delete(t.get());
    	}
    }

    public void edit(ID id, T model) {
    	Optional<T> t = this.repository.findById(id);
    	if(t.isPresent()) {
    		model.setId(t.get().getId());
    		this.repository.save(model);
    	}
    }

}