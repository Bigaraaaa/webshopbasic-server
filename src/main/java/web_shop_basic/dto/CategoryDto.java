package web_shop_basic.dto;

public class CategoryDto extends AbstractDto {

	private String name;
	private String description;
	
	public CategoryDto() {
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	
}
