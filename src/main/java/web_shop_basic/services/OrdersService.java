package web_shop_basic.services;

import java.util.HashSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import web_shop_basic.models.Orders;
import web_shop_basic.models.Product;
import web_shop_basic.repositories.OrdersRepository;
import web_shop_basic.repositories.ProductRepository;

@Service
public class OrdersService extends GenericService<Orders, Long> {

	@Autowired
	private OrdersRepository ordersRepository;
	
	@Autowired
	private ProductRepository productRepozitory;
	
//	public Orders addToCart(Orders orders) {
//		System.out.println(orders);
//		return ordersRepository.save(orders);
//	}
}
