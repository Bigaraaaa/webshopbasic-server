package web_shop_basic.security;


import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import web_shop_basic.models.AccountData;
import web_shop_basic.repositories.AccountDataRepository;

@Service
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    private AccountDataRepository accountDataRepository;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String email)throws UsernameNotFoundException {
        AccountData accountData = accountDataRepository.findByEmail(email).orElseThrow(() -> new UsernameNotFoundException("User not found with email : " + email)
        );

        return UserPrincipal.create(accountData);
    }

    @Transactional
    public UserDetails loadUserById(Long id) {
    	Optional<AccountData> accountData = accountDataRepository.findById(id);
        return UserPrincipal.create(accountData.get());
   }
}