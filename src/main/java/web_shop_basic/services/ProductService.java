package web_shop_basic.services;

import org.springframework.stereotype.Service;

import web_shop_basic.models.Product;

@Service
public class ProductService extends GenericService<Product, Long> {

}
