package web_shop_basic.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import web_shop_basic.dto.SizeDto;
import web_shop_basic.models.Size;
import web_shop_basic.services.GenericService;

@RestController
@RequestMapping(path = "/size")
public class SizeController extends GenericController<SizeDto, Size, Long> {

	public SizeController(GenericService<Size, Long> service) {
		super(service, SizeDto.class);
		// TODO Auto-generated constructor stub
	}

}
