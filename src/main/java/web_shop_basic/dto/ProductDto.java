package web_shop_basic.dto;

public class ProductDto extends AbstractDto {

	private ProductDetailsDto productDetailsDto;
	private CategoryDto category;
	
	public ProductDto() {
		
	}

	public ProductDetailsDto getProductDetailsDto() {
		return productDetailsDto;
	}

	public void setProductDetailsDto(ProductDetailsDto productDetailsDto) {
		this.productDetailsDto = productDetailsDto;
	}

	public CategoryDto getCategory() {
		return category;
	}

	public void setCategory(CategoryDto category) {
		this.category = category;
	}

		
}
