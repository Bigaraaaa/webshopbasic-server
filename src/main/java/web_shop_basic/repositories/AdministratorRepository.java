package web_shop_basic.repositories;

import org.springframework.stereotype.Repository;

import web_shop_basic.models.Administrator;

@Repository
public interface AdministratorRepository extends GenericRepository<Administrator, Long> {

}
