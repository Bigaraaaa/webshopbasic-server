package web_shop_basic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebShopBasic {

	public static void main(String[] args) {
		SpringApplication.run(WebShopBasic.class, args);
	}

}
