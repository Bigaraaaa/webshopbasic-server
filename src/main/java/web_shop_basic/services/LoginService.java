package web_shop_basic.services;

import java.util.HashMap;
import java.util.HashSet;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import web_shop_basic.models.AccountData;
import web_shop_basic.models.UserPermission;
import web_shop_basic.repositories.AccountDataRepository;
import web_shop_basic.repositories.PermissionRepository;
import web_shop_basic.utils.TokenProvider;

@Service
public class LoginService {

	@Autowired
	private AccountDataRepository accountRepository;

	@Autowired
	private AuthenticationManager authenticationManager;
	
	@Autowired
	private TokenProvider tokenProvider;
	
	@Autowired
	private PermissionRepository permissionRepository;
	
	
	public ResponseEntity<HashMap<String, String>> authenticateUser(@Valid @RequestBody AccountData loginRequest) {

		try {
			Authentication authentication = authenticationManager.authenticate( new UsernamePasswordAuthenticationToken(loginRequest.getEmail(), loginRequest.getPassword()));
	
	        SecurityContextHolder.getContext().setAuthentication(authentication);

	        String token = tokenProvider.createToken(authentication);
	        
	        HashMap<String, String> data = new HashMap<String, String>();
	        
			data.put("accessToken", token);
			
	        return new ResponseEntity<HashMap<String, String>>(data, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<HashMap<String, String>>(HttpStatus.UNAUTHORIZED);
		}
	}
	
	public void addPermssion(AccountData accountData, String authority) {
		accountData = accountRepository.save(accountData);
		accountData.setUserPermission(new HashSet<UserPermission>());
		accountData.getUserPermission().add(new UserPermission(null, accountData, permissionRepository.findByAuthority(authority).get()));	
		accountRepository.save(accountData);
	}
	

}
