package web_shop_basic.repositories;

import org.springframework.stereotype.Repository;

import web_shop_basic.models.Product;

@Repository
public interface ProductRepository extends GenericRepository<Product, Long> {

}
